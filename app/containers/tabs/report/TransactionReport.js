//import liraries
import React, { Component } from 'react';
import {
  ImageBackground,
  View,
  Text,
  StyleSheet,
  Platform,
  TouchableOpacity,
  Image
} from 'react-native';

import images from '../../../const/images';

import {
  updateStackNavigation
} from '../../../global';

// create a component
class TransactionReport extends Component {
  constructor(props) {
    super(props);

    updateStackNavigation(props.navigation, 2);
  }

  render() {
    return (
      <ImageBackground
        source={images.background2}
        style={styles.container}
        resizeMode='cover'
      >
        <View style={styles.header}>
          <TouchableOpacity
            style={styles.headerLeftButton}
            onPress={() => this.props.navigation.goBack()}
          >
            <Image
              source={images.back}
              style={styles.headerLeftIcon}
              resizeMode='contain'
            />
          </TouchableOpacity>

          <Text style={styles.headerText}>Transaction Report</Text>
        </View>

        <View style={styles.content}>
          <View style={styles.statusView}>
            <View>
              <View style={{flexDirection: 'row'}}>
                <Text style={[styles.successText1, {width: 120}]}>Total Success:</Text>
                <Text style={styles.successText2}>RM30</Text>
              </View>

              <View style={{flexDirection: 'row'}}>
                <Text style={[styles.failedText1, {width: 120}]}>Total Failed:</Text>
                <Text style={styles.failedText2}>RM10</Text>
              </View>

              <View style={{flexDirection: 'row'}}>
                <Text style={[styles.grandText1, {width: 120}]}>Total Grand:</Text>
                <Text style={styles.grandText2}>RM40</Text>
              </View>
            </View>

            <TouchableOpacity
            >
              <ImageBackground
                source={images.buttonBG1}
                style={styles.button}
                resizeMode='stretch'
              >
                <Text style={styles.buttonText}>Refresh</Text>
              </ImageBackground>
            </TouchableOpacity>
          </View>

          <View style={styles.transactionList}>

            <View style={styles.transactionView}>
              <Text style={styles.transactionIDText}>S_ID: 2984748489</Text>
              <Text style={styles.transactionTimeText}>28 Feb 2018 | 9:50pm</Text>
              <View style={styles.digiInfo}>
                <Image
                  source={images.digiIcon}
                  style={styles.digiIcon}
                  resizeMode='contain'
                />
                <View>
                  <Text style={styles.digiText}>DIGI</Text>
                  <Text style={styles.digiIDText}>93838474734535556434</Text>
                  <Text style={styles.digiStatusText}>Reload Success</Text>
                </View>
              </View>
            </View>

            <View style={styles.transactionView}>
              <Text style={styles.transactionIDText}>S_ID: 2984748490</Text>
              <Text style={styles.transactionTimeText}>28 Feb 2018 | 9:40pm</Text>
              <View style={styles.digiInfo}>
                <Image
                  source={images.digiIcon}
                  style={styles.digiIcon}
                  resizeMode='contain'
                />
                <View>
                  <Text style={styles.digiText}>DIGI</Text>
                  <Text style={styles.digiIDText}>92837636262629292028</Text>
                  <Text style={styles.digiStatusText}>Reload Failed. Invalid Credit card payment</Text>
                </View>
              </View>
            </View>

          </View>
        </View>
      </ImageBackground>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Platform.select({ ios: 20 }),
    backgroundColor: 'transparent'
  },
  header: {
    width: '100%',
    height: 55,
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerLeftButton: {
    marginLeft: 12,
    marginRight: 12,
  },
  headerLeftIcon: {
    width: 20,
    height: 20
  },
  headerText: {
    color: 'white',
    fontSize: 18,
    fontFamily: 'Myriad-Italic',
  },
  content: {
    flex: 1,
    backgroundColor: '#E1E1E1'
  },
  statusView: {
    width: '100%',
    padding: 20,
    paddingLeft: 25,
    paddingRight: 25,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderBottomColor: '#6E7072',
  },
  successText1: {
    color: '#88C247',
    fontSize: 17,
    fontFamily: 'Myriad-Roman',
  },
  successText2: {
    color: '#88C247',
    fontSize: 17,
    fontFamily: 'Myriad-Bold',
  },
  failedText1: {
    color: '#FF0000',
    fontSize: 17,
    fontFamily: 'Myriad-Roman',
  },
  failedText2: {
    color: '#FF0000',
    fontSize: 17,
    fontFamily: 'Myriad-Bold',
  },
  grandText1: {
    color: '#231F20',
    fontSize: 17,
    fontFamily: 'Myriad-Roman',
  },
  grandText2: {
    color: '#231F20',
    fontSize: 17,
    fontFamily: 'Myriad-Bold',
  },
  button: {
    width: 130,
    height: 35,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    color: 'white',
    fontSize: 17,
    fontFamily: 'Myriad-Italic',
    backgroundColor: 'transparent'
  },
  transactionList: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20
  },
  transactionView: {
    width: '100%',
    height: 130,
    padding: 10,
    paddingLeft: 15,
    paddingRight: 0,
    borderRadius: 10,
    backgroundColor: 'white',
    marginTop: 15,
  },
  transactionIDText: {
    marginBottom: 5,
    color: '#231F20',
    fontSize: 17,
    fontFamily: 'Myriad-Roman',
  },
  transactionTimeText: {
    color: '#231F20',
    fontSize: 14,
    fontFamily: 'Myriad-Italic',
  },
  digiInfo: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  digiIcon: {
    width: 90,
    height: 55,
  },
  digiText: {
    color: '#231F20',
    fontSize: 13,
    fontFamily: 'Myriad-Roman',
  },
  digiIDText: {
    color: '#231F20',
    fontSize: 13,
    fontFamily: 'Myriad-Roman',
  },
  digiStatusText: {
    color: '#FF0000',
    fontSize: 13,
    fontFamily: 'Myriad-Italic',
  }
});

//make this component available to the app
export default TransactionReport;
