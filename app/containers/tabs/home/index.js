//import liraries
import React from 'react';
import { StackNavigator } from 'react-navigation';

import Home from './home';
import MobileTopUpScreen from './MobileTopUp';
import MobileTopUpDetailScreen from './MobileTopUp/detail';
import MobileTopUpResultScreen from './MobileTopUp/result';
import InternationalTopUpScreen from './InternationalTopUp';
import InternationalTopUpDetailScreen from './InternationalTopUp/detail';
import PayCreditCard from './MobileTopUp/PayCreditCard';
import UtilitiesScreen from './Utilities';
import UtilityDetailScreen from './Utilities/detail';
import BuyCredit from './BuyCredit';
import BuyCreditOnline from './BuyCredit/BuyCreditOnline';
import CreditTransfer from './CreditTransfer';

import { transitionConfig } from '../../../global';

const HomeNavigator = StackNavigator({
  Home: {
    screen: Home
  },
  MobileTopUp: {
    screen: MobileTopUpScreen
  },
  MobileTopUpDetail: {
    screen: MobileTopUpDetailScreen
  },
  MobileTopUpResult: {
    screen: MobileTopUpResultScreen
  },
  InternationalTopUp: {
    screen: InternationalTopUpScreen
  },
  InternationalTopUpDetail: {
    screen: InternationalTopUpDetailScreen
  },
  PayCreditCard: {
    screen: PayCreditCard
  },
  Utilities: {
    screen: UtilitiesScreen
  },
  UtilityDetail: {
    screen: UtilityDetailScreen
  },
  BuyCredit: {
    screen: BuyCredit
  },
  BuyCreditOnline: {
    screen: BuyCreditOnline
  },
  CreditTransfer: {
    screen: CreditTransfer
  }
}, {
  headerMode: 'none',
  initialRouteName: 'Home',
  navigationOptions: {
    gesturesEnabled: false,
  },
  transitionConfig
})

export default HomeNavigator;