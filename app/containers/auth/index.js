//import liraries
import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  Image,
  TouchableOpacity,
  ScrollView,
  TextInput
} from 'react-native';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { loginRequest } from '../../actions/user';

import images from '../../const/images';

// create a component
class AuthScreen extends Component {
  constructor() {
    super();

    this.state = {
      screen: 0,     //1: SignIn, 2: SignUp, 3: Forget Password
      login_email: 'vla_afansyev@mail.ru',
      login_password: 'password',
      isFetching: false,
    }
  }

  login() {
    const { loginRequest } = this.props;
    const { login_email, login_password } = this.state;

    loginRequest(login_email, login_password);
  }

  componentWillReceiveProps(nextProps) {
    const { isFetching } = this.state;

    if (isFetching) {
      alert(nextProps.errorMsg);
    }
    this.setState({isFetching: nextProps.isFetching});
  }

  renderGate() {
    return (
      <View style={styles.content}>
        <Image
          source={images.logo}
          style={styles.logo}
          resizeMode='contain'
        />
        <Text style={styles.whiteText}>START SENZPAY TODAY!</Text>
        <TouchableOpacity
          style={[styles.button, { backgroundColor: 'transparent' }]}
          onPress={() => this.setState({ screen: 1 })}
        >
          <Text style={styles.buttonText1}>LOGIN</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.button, { backgroundColor: 'white' }]}
          onPress={() => alert('singup')}
        >
          <Text style={styles.buttonText2}>SIGN UP</Text>
        </TouchableOpacity>
        <View />
      </View>
    )
  }

  renderSignIn() {
    const { login_email, login_password } = this.state;
    return (
      <View style={styles.innerContainer}>
        <Image
          source={images.logo}
          style={styles.logo}
          resizeMode='contain'
        />

        <View style={styles.customInput}>
          <Image
            source={images.username}
            style={styles.inputIcon}
            resizeMode='contain'
          />
          <TextInput
            style={styles.input}
            placeholder='USERNAME'
            value={login_email}
            placeholderTextColor='white'
            onChangeText={text => this.setState({login_email: text})}
            underlineColorAndroid='transparent'
            autoCapitalize='none'
          />
        </View>

        <View style={styles.customInput}>
          <Image
            source={images.password}
            style={styles.inputIcon}
            resizeMode='contain'
          />
          <TextInput
            style={styles.input}
            placeholder='PASSWORD'
            placeholderTextColor='white'
            value={login_password}
            onChangeText={text => this.setState({login_password: text})}
            underlineColorAndroid='transparent'
            secureTextEntry
          />
        </View>

        <TouchableOpacity
          style={[styles.button, {marginTop: 50, backgroundColor: 'white'}]}
          onPress={this.login.bind(this)}
        >
          <Text style={styles.buttonText2}>LOGIN</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.forgotButton}
          onPress={() => alert('forget password')}
        >
          <Text style={styles.linkText1}>FORGOT PASSWORD?</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.signupButton}
          onPress={() => alert('sign up')}
        >
          <Text style={styles.linkText1}>NO ACCOUNT? </Text>
          <Text style={styles.linkText2}>SIGN UP</Text>
        </TouchableOpacity>
      </View>
    )
  }

  renderSignUp() {

  }

  render() {
    const { screen } = this.state;

    return (
      <ScrollView contentContainerStyle={styles.outerContainer} scrollEnabled={false}>
        <ImageBackground
          source={images.background1}
          style={styles.background}
          resizeMode='cover'
        >
          {
            screen === 0 ?
            this.renderGate()
            : screen === 1 ?
            this.renderSignIn()
            : this.renderSignUp()
          }
        </ImageBackground>
      </ScrollView>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  outerContainer: {
    flex: 1,
  },
  background: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    width: 240,
    height: 350,
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: 'transparent'
  },
  innerContainer: {
    width: 240,
    alignItems: 'center',
    backgroundColor: 'transparent'
  },
  logo: {
    width: '100%',
    height: 100
  },
  whiteText: {
    color: 'white',
    fontSize: 16,
    fontFamily: 'Montserrat-Light',
  },
  buttonText1: {
    color: 'white',
    fontSize: 17,
    fontFamily: 'Myriad-Italic',
  },
  buttonText2: {
    color: 'black',
    fontSize: 17,
    fontFamily: 'Myriad-Italic',
  },
  button: {
    width: '100%',
    height: 40,
    borderRadius: 20,
    borderWidth: 2.5,
    borderColor: 'white',
    alignItems: 'center',
    justifyContent: 'center'
  },
  forgotButton: {
    marginTop: 30,
  },
  signupButton: {
    marginTop: 50,
    flexDirection: 'row',
  },
  linkText1: {
    color: 'white',
    fontSize: 13,
    fontFamily: 'Myriad-Italic'
  },
  linkText2: {
    color: 'white',
    fontSize: 13,
    fontFamily: 'Myriad-BoldItalic'
  },
  customInput: {
    width: '100%',
    height: 50,
    borderBottomWidth: 1.5,
    borderBottomColor: 'white',
    marginTop: 60,
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputIcon: {
    width: 25,
    height: '100%',
    marginRight: 35,
  },
  input: {
    flex: 1,
    padding: 0,
    color: 'white',
    fontSize: 17,
    fontFamily: 'Myriad-Italic',
  }
});

const mapStateToProps = function (state) {
  const { user } = state;
  return {
    isFetching: user.isFetching,
    errorMsg: user.errorMsg
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    loginRequest
  }, dispatch);
}

//make this component available to the app
export default connect(mapStateToProps, mapDispatchToProps)(AuthScreen);
